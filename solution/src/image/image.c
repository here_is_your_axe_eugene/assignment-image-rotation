//
// Created by Maxim on 26.12.2021.
//

#include "image.h"
#include <malloc.h>

struct image_struct image_create(uint64_t const width, uint64_t const height) {
    struct pixel *const pixels = malloc(width * height * sizeof(struct pixel));
    return (struct image_struct) {width, height, pixels};
}

void image_destroy(struct image_struct const image) {
    free(image.data);
}
