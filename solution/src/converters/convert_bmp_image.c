//
// Created by Maxim on 26.12.2021.
//

#include "convert_bmp_image.h"

struct image_struct bmp_to_image(struct bmp_struct const source) {
    struct image_struct image = {source.header.biWidth, source.header.biHeight, source.pixels};
    return image;
}

struct bmp_struct image_to_bmp(struct image_struct const source, struct bmp_header header) {
    header.biHeight = source.height;
    header.biWidth = source.width;
    return (struct bmp_struct) {header, source.data};
}
