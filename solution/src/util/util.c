//
// Created by Maxim on 26.12.2021.
//

#include "util.h"
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>


/**
 * https://gitlab.se.ifmo.ru/programming-languages/bmp_struct-header-viewer/-/blob/master/util.c
 */
_Noreturn void err(const char *msg, ...) {
    va_list args;
    va_start (args, msg);
    // There is a bug in clang-tidy that makes it consider args as uninitialized
    // NOLINT helps supress this message
    // See: https://bugs.llvm.org/show_bug.cgi?id=41311
    vfprintf(stderr, msg, args); // NOLINT
    va_end (args);
    exit(1);
}



