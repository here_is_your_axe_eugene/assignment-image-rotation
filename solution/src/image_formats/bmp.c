//
// Created by Maxim on 26.12.2021.
//

#include "bmp.h"
#include <malloc.h>


struct bmp_struct bmp_create(struct bmp_header const header) {
    const uint32_t width = header.biWidth;
    const uint32_t height = header.biHeight;
    const uint32_t size = height * width;

    struct pixel *pixels = malloc(size * sizeof(struct pixel));
    return (struct bmp_struct) {header, pixels};
}
