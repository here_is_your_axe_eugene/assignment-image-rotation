#include "util.h"

#include "bmp_io.h"
#include "convert_bmp_image.h"
#include "image_rotate.h"
#include <errno.h>
#include <stdio.h>
#include <string.h>


void usage(const char *path) {
    err("Usage: %s in_file.bmp out_file.bmp\n", path);
}

void check_file(FILE *file, const char *name) {
    if (!file) {
        err("Can't process the file \"%s\": %s\n", name, strerror(errno));
    }
}

int main(int argc, char **argv) {
    if (argc != 3) usage(argv[0]);
    if (argc < 3) err("Not enough arguments\n");
    if (argc > 3) err("Too many arguments\n");

    char __attribute__((unused)) *file_in_name = argv[1];
    char __attribute__((unused)) *file_out_name = argv[2];  // ¯\_(ツ)_/¯ suppressed warning

    FILE *read = fopen(file_in_name, "rb");
    check_file(read, file_in_name);

    struct bmp_struct bmp = bmp_from_file(read);
    fclose(read);

    struct image_struct image = bmp_to_image(bmp);
    struct image_struct rotated_image = rotate(image);
    image_destroy(image);

    struct bmp_struct bmp2 = image_to_bmp(rotated_image, bmp.header);

    FILE *write = fopen(file_out_name, "wb");
    check_file(write, file_out_name);

    bmp_to_file(write, bmp2);
    fclose(write);
    image_destroy(rotated_image);

    printf("Functions worked out well. Check output.\n");

    return 0;
}
