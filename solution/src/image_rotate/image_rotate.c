//
// Created by Maxim on 26.12.2021.
//

#include "image_rotate.h"

struct image_struct rotate(struct image_struct const source) {
    struct image_struct const image = image_create(source.height, source.width);

    for (uint64_t x = 0; x < image.width; ++x) {
        for (uint64_t y = 0; y < image.height; ++y) {
            image.data[get_pixel_idx(image, image.width - x - 1, y)] = source.data[get_pixel_idx(source, y, x)];
        }
    }

    return image;
}
