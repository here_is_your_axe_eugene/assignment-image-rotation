//
// Created by Maxim on 26.12.2021.
//

#include "bmp_io.h"
#include "bmp.h"
#include "util.h"

void check_file_for_error(FILE *const file) {
    int err_code = ferror(file);
    if (err_code) {
        err("Error while processing the file. Code: %d\n", err_code);
    }
}

struct bmp_header bmp_header_from_file(FILE *const file) {
    struct bmp_header header = {0};
    fread(&header, sizeof(struct bmp_header), 1, file);
    check_file_for_error(file);
    if ((uint8_t) header.bfType != 'B' && (uint8_t) header.bfType >> 8 != 'M')
        err("Your file is not in BMP format.\n");
    if (header.biBitCount != 24)
        err("This program supports BMPs with only 24 bit count. Your is %d\n", header.biBitCount);
    return header;
}

struct bmp_struct bmp_from_file(FILE *const file) {
    // Reading header from the given file and initiating bmp_struct
    const struct bmp_header header = bmp_header_from_file(file);
    struct bmp_struct bmp = bmp_create(header);

    const uint32_t width = header.biWidth;
    const uint32_t height = header.biHeight;
    const uint8_t padding = calculate_padding(width);

    // Reading pixels from the given file
    for (int i = 0; i < height; ++i) {
        fread(bmp.pixels + i * width, sizeof(struct pixel), width, file);
        check_file_for_error(file);
        fseek(file, padding, SEEK_CUR); // Padding must be removed from every row
        check_file_for_error(file);
    }
    return bmp;
}


void bmp_to_file(FILE *const file, struct bmp_struct const bmp) {
    const uint32_t width = bmp.header.biWidth;
    const uint32_t height = bmp.header.biHeight;
    const uint8_t padding = calculate_padding(width);
    const char padding_byte[4] = {'b', 'y', 't', 'e'}; // just random byte to write into padding

    // Writing header
    fwrite(&bmp.header, sizeof(struct bmp_header), 1, file);

    // Writing pixels into the file
    for (int i = 0; i < height; ++i) {
        fwrite(bmp.pixels + i * width, sizeof(struct pixel), width, file);
        check_file_for_error(file);
        fwrite(&padding_byte, 1, padding, file); // With padding
        check_file_for_error(file);
    }
}
