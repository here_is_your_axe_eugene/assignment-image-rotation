//
// Created by Maxim on 26.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_CONVERT_BMP_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_CONVERT_BMP_IMAGE_H

#include "bmp.h"
#include "image.h"

struct image_struct bmp_to_image(struct bmp_struct source);

/**
 * I decided to make it by copying the previous header and changing the width and height
 * @param source - image to convert
 * @param header
 * @return ready to write bpm_struct
 */
struct bmp_struct image_to_bmp(struct image_struct source, struct bmp_header header);

#endif //ASSIGNMENT_IMAGE_ROTATION_CONVERT_BMP_IMAGE_H
