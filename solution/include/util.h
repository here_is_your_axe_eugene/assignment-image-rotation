//
// Created by Maxim on 26.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_UTIL_H
#define ASSIGNMENT_IMAGE_ROTATION_UTIL_H

#include <stdint.h>

/**
 * https://gitlab.se.ifmo.ru/programming-languages/bmp_struct-header-viewer/-/blob/master/util.c
 */
_Noreturn void err(const char *msg, ...);

#endif //ASSIGNMENT_IMAGE_ROTATION_UTIL_H
