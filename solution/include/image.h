//
// Created by Maxim on 26.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <stdint.h>
#include <stdio.h>
#include <malloc.h>
#include "pixel.h"

struct image_struct {
    uint64_t width, height;
    struct pixel *data;
};

struct image_struct image_create(uint64_t width, uint64_t height);

void image_destroy(struct image_struct image);

static inline size_t get_pixel_idx(struct image_struct image, uint64_t width, uint64_t height) {
    return image.width * height + width;
}

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
