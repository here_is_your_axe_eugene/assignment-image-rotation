//
// Created by Maxim on 26.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_ROTATE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_ROTATE_H

#include "image.h"

/* Создаёт повёрнутую копию изображения */
struct image_struct rotate(struct image_struct source);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_ROTATE_H
