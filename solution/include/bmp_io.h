//
// Created by Maxim on 26.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_IO_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_IO_H

#include <stdio.h>
#include <stdbool.h>
#include "bmp.h"

struct bmp_struct bmp_from_file(FILE *file);

void bmp_to_file(FILE *file, struct bmp_struct bmp);

static inline uint8_t calculate_padding(uint32_t width) {
    return (4 - width * 3 % 4) % 4;
}

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_IO_H
