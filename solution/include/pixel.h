//
// Created by Maxim on 26.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_PIXEL_H
#define ASSIGNMENT_IMAGE_ROTATION_PIXEL_H

#include <stdint.h>

struct __attribute__ ((packed)) pixel {
    uint8_t b, g, r;
};

#endif //ASSIGNMENT_IMAGE_ROTATION_PIXEL_H
